# PROYECTO: GESTOR USUARIOS

## Descripción

Crear un servicio REST en Java que gestione usuarios con operaciones de GET y POST.
Utilizar Spring Boot para levantar el servicio REST y Maven como sistema de gestión y
construcción del proyecto.

## Requisitos

* Java JDK versión 11 o superior 
* Maven para la gestión de dependencias y construcción del proyecto 
* Spring Boot para la creación del servicio REST 
* Postman u otro para probar los endpoints

## Herramientas

* IDE: Intellij
* Lenguaje: Java
    * Versión: 17 (versión 21, actualmente no soportado por IntelliJ)
* Framework: Spring
* Controlador de versiones: Gitlab
    * Url del repositorio: https://gitlab.com/carlosjesuspm/gestorusuario_experienceittest.git
* Design Patterns: Factory (Empleado para la creación de usuarios)
* Documentación:
    * Swagger(OpenApi) Versión: 2.3.0
        * Enlaces:
            * HMTL: http://localhost:8080/swagger-ui/index.html#/
            * JSON: http://localhost:8080/v2/api-docs
* Testing de integración:
    * Postman
* Depedencias:
    * JPA
    * H2 Database
    * SpringDevTools
    * SpringBootStarterTest
    * SpringBootWeb
    * ModelMapper (conversión de entities a DTOs)
        * Versión: 3.2.2 (Octubre, 2023)
    * SpringDoc-OpenApi



