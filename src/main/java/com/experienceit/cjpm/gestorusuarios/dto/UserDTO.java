package com.experienceit.cjpm.gestorusuarios.dto;

import com.experienceit.cjpm.gestorusuarios.model.User;

import java.io.Serial;
import java.io.Serializable;

public class UserDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    //Atributos
    private int idUsuario;

    private String nombreUsuario;
    private String apellido1Usuario;
    private String apellido2Usuario;
    private boolean mayoriaEdadUsuario;
    private String provinciaUsuario;
    private String dniUsuario;
    private String contrasenyaUsuario;

    //Constructor

    public UserDTO() {
    }

    public UserDTO(User usuario) {
        this.idUsuario = usuario.getIdUsuario();
        this.nombreUsuario = usuario.getNombreUsuario();
        this.apellido1Usuario = usuario.getApellido1Usuario();
        this.apellido2Usuario = usuario.getApellido2Usuario();
        this.mayoriaEdadUsuario = usuario.isMayoriaEdadUsuario();
        this.provinciaUsuario = usuario.getProvinciaUsuario();
        this.dniUsuario = usuario.getDNIUsuario();
        this.contrasenyaUsuario = usuario.getContrasenyaUsuario();
    }

    public UserDTO(int idUsuario, String nombreUsuario, String apellido1Usuario, String apellido2Usuario, boolean mayoriaEdadUsuario, String provinciaUsuario, String dniUsuario, String contrasenyaUsuario) {
        this.idUsuario = idUsuario;
        this.nombreUsuario = nombreUsuario;
        this.apellido1Usuario = apellido1Usuario;
        this.apellido2Usuario = apellido2Usuario;
        this.mayoriaEdadUsuario = mayoriaEdadUsuario;
        this.provinciaUsuario = provinciaUsuario;
        this.dniUsuario = dniUsuario;
        this.contrasenyaUsuario = contrasenyaUsuario;
    }


    //Getters y Setters

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellido1Usuario() {
        return apellido1Usuario;
    }

    public void setApellido1Usuario(String apellido1Usuario) {
        this.apellido1Usuario = apellido1Usuario;
    }

    public String getApellido2Usuario() {
        return apellido2Usuario;
    }

    public void setApellido2Usuario(String apellido2Usuario) {
        this.apellido2Usuario = apellido2Usuario;
    }

    public boolean isMayoriaEdadUsuario() {
        return mayoriaEdadUsuario;
    }

    public void setMayoriaEdadUsuario(boolean mayoriaEdadUsuario) {
        this.mayoriaEdadUsuario = mayoriaEdadUsuario;
    }

    public String getProvinciaUsuario() {
        return provinciaUsuario;
    }

    public void setProvinciaUsuario(String provinciaUsuario) {
        this.provinciaUsuario = provinciaUsuario;
    }

    public String getDniUsuario() {
        return dniUsuario;
    }

    public void setDniUsuario(String dniUsuario) {
        this.dniUsuario = dniUsuario;
    }

    public String getContrasenyaUsuario() {
        return contrasenyaUsuario;
    }

    public void setContrasenyaUsuario(String contrasenyaUsuario) {
        this.contrasenyaUsuario = contrasenyaUsuario;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "nombreUsuario='" + nombreUsuario + '\'' +
                ", apellido1Usuario='" + apellido1Usuario + '\'' +
                ", apellido2Usuario='" + apellido2Usuario + '\'' +
                ", mayoriaEdadUsuario=" + mayoriaEdadUsuario +
                ", provinciaUsuario='" + provinciaUsuario + '\'' +
                ", DNIUsuario='" + dniUsuario + '\'' +
                ", ContrasenyaUsuario='" + contrasenyaUsuario + '\'' +
                '}';
    }
}
