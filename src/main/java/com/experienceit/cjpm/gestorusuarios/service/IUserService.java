package com.experienceit.cjpm.gestorusuarios.service;

import com.experienceit.cjpm.gestorusuarios.model.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    User saveUser(User usuario);
    List<User> findAllUsers();
    Optional<User> findUserById(int id);
    boolean deleteAllUsers();
    boolean deleteUserById(int id);


}
