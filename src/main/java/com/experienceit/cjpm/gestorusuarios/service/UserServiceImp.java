package com.experienceit.cjpm.gestorusuarios.service;

import com.experienceit.cjpm.gestorusuarios.model.User;
import com.experienceit.cjpm.gestorusuarios.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImp  implements IUserService {


    //Atributos
    private final UserRepository userRepository;

    //Constructor
    @Autowired
    public UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //Getter

    public UserRepository getUserRepository() {
        return userRepository;
    }

    // Implementación de métodos

    /**
     * Método encargado de crear y guardar un usuario
     * @param usuario -
     * @return User
     */
    @Override
    public User saveUser(User usuario) {
        return userRepository.save(usuario);
    }

    /**
     * Método encargado de devolver a todos los usuarios
     * @return List<User>
     */

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Método encargado de devolver usuario según su id
     * @param id -
     * @return Optional<User>
     */
    @Override
    public Optional<User> findUserById(int id) {
        if(id<=0){
            return Optional.empty();
        }
        return userRepository.findById(id);
    }

    /**
     * Método encargado de borrar todos los usuarios
     * @return true
     */
    @Override
    public boolean deleteAllUsers() {
        userRepository.deleteAll();
        return true;
    }

    /**
     * Método encargado de borrar un usuario determinado
     * @param id -
     * @return true
     */
    @Override
    public boolean deleteUserById(int id) {
        if(!userRepository.existsById(id)){
           return false;
        }
        userRepository.deleteById(id);
        return true;

    }
}
