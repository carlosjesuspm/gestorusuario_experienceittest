package com.experienceit.cjpm.gestorusuarios.factory;

import com.experienceit.cjpm.gestorusuarios.dto.UserDTO;
import com.experienceit.cjpm.gestorusuarios.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserFactoryImp implements IUserFactory{


    /**
     * Método encargado de instanciar un usuario
     * @param usuario -
     * @return UserDTO
     */
    @Override
    public UserDTO crearUserDTO(User usuario) {
        return new UserDTO(usuario);
    }
}
