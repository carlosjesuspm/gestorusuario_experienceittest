package com.experienceit.cjpm.gestorusuarios.factory;

import com.experienceit.cjpm.gestorusuarios.dto.UserDTO;
import com.experienceit.cjpm.gestorusuarios.model.User;

public interface IUserFactory {

    UserDTO crearUserDTO(User usuario);
}
