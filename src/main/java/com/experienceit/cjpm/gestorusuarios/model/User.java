package com.experienceit.cjpm.gestorusuarios.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;


@Entity
@Table(name="usuarios")
public class User {

    //Atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_usuario")
    private int idUsuario;

    @Column(name = "nombre_usuario")
    @NotNull
    @NotEmpty
    private String nombreUsuario;

    @Column(name = "apellido1_usuario")
    @NotNull
    @NotEmpty
    private String apellido1Usuario;

    @Column(name = "apellido2_usuario")
    @NotNull
    @NotEmpty
    private String apellido2Usuario;

    @Column(name = "mayoriaEdad_usuario")
    @NotNull
    private boolean mayoriaEdadUsuario;

    @Column(name = "provincia_usuario")
    @NotNull
    @NotEmpty
    private String provinciaUsuario;

    @Column(name = "dni_usuario")
    @NotNull
    @NotEmpty
    private String DNIUsuario;

    @Column(name = "password_usuario")
    @NotNull
    @NotEmpty
    private String ContrasenyaUsuario;

    //Constructor

    public User() {
    }

    public User(int idUsuario, String nombreUsuario, String apellido1Usuario, String apellido2Usuario, boolean mayoriaEdadUsuario, String provinciaUsuario, String DNIUsuario, String contrasenyaUsuario) {
        this.idUsuario = idUsuario;
        this.nombreUsuario = nombreUsuario;
        this.apellido1Usuario = apellido1Usuario;
        this.apellido2Usuario = apellido2Usuario;
        this.mayoriaEdadUsuario = mayoriaEdadUsuario;
        this.provinciaUsuario = provinciaUsuario;
        this.DNIUsuario = DNIUsuario;
        ContrasenyaUsuario = contrasenyaUsuario;
    }


    //Getters y Setters

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellido1Usuario() {
        return apellido1Usuario;
    }

    public void setApellido1Usuario(String apellido1Usuario) {
        this.apellido1Usuario = apellido1Usuario;
    }

    public String getApellido2Usuario() {
        return apellido2Usuario;
    }

    public void setApellido2Usuario(String apellido2Usuario) {
        this.apellido2Usuario = apellido2Usuario;
    }

    public boolean isMayoriaEdadUsuario() {
        return mayoriaEdadUsuario;
    }

    public void setMayoriaEdadUsuario(boolean mayoriaEdadUsuario) {
        this.mayoriaEdadUsuario = mayoriaEdadUsuario;
    }

    public String getProvinciaUsuario() {
        return provinciaUsuario;
    }

    public void setProvinciaUsuario(String provinciaUsuario) {
        this.provinciaUsuario = provinciaUsuario;
    }

    public String getDNIUsuario() {
        return DNIUsuario;
    }

    public void setDNIUsuario(String DNIUsuario) {
        this.DNIUsuario = DNIUsuario;
    }

    public String getContrasenyaUsuario() {
        return ContrasenyaUsuario;
    }

    public void setContrasenyaUsuario(String contrasenyaUsuario) {
        ContrasenyaUsuario = contrasenyaUsuario;
    }

    @Override
    public String toString() {
        return "User{" +
                "nombreUsuario='" + nombreUsuario + '\'' +
                ", apellido1Usuario='" + apellido1Usuario + '\'' +
                ", apellido2Usuario='" + apellido2Usuario + '\'' +
                ", mayoriaEdadUsuario=" + mayoriaEdadUsuario +
                ", provinciaUsuario='" + provinciaUsuario + '\'' +
                ", DNIUsuario='" + DNIUsuario + '\'' +
                ", ContrasenyaUsuario='" + ContrasenyaUsuario + '\'' +
                '}';
    }
}
