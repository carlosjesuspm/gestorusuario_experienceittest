package com.experienceit.cjpm.gestorusuarios.controller;

import com.experienceit.cjpm.gestorusuarios.config.DTOConverter;
import com.experienceit.cjpm.gestorusuarios.dto.UserDTO;
import com.experienceit.cjpm.gestorusuarios.exception.UserBadRequestException;
import com.experienceit.cjpm.gestorusuarios.exception.UserGlobalException;
import com.experienceit.cjpm.gestorusuarios.exception.UserNoContentException;
import com.experienceit.cjpm.gestorusuarios.exception.UserNotFoundException;
import com.experienceit.cjpm.gestorusuarios.factory.UserFactoryImp;
import com.experienceit.cjpm.gestorusuarios.model.User;
import com.experienceit.cjpm.gestorusuarios.service.UserServiceImp;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class UserController {

    //Atributos
    private final UserServiceImp userServiceImp;
    private final UserFactoryImp userFactoryImp;
    private final DTOConverter dtoConverter;

    //Constructor
    @Autowired
    public UserController(UserServiceImp userServiceImp,UserFactoryImp userFactoryImp, DTOConverter dtoConverter) {
        this.userServiceImp = userServiceImp;
        this.userFactoryImp=userFactoryImp;
        this.dtoConverter=dtoConverter;

    }

    //Endpoints
    /**
     * Endpoint para la devolución de todos los usuarios
     * @return ResponseEntity<List<UserDTO>>
     */

    @Operation(summary = "Mostrar listado de usuarios")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Listado encontrado",content = {@Content(mediaType="application/json",schema=@Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "204", description = "Lista devuelta vacía", content = @Content)
    })

    @GetMapping("/usuarios")
    public ResponseEntity<List<UserDTO>> encontrarTodosUsuarios(){
        List<User> listadoTotalUsuarios = userServiceImp.findAllUsers();

        if(listadoTotalUsuarios ==null|| listadoTotalUsuarios.isEmpty()){
            throw new UserNoContentException("La lista de usuarios se encuentra vacía");
        }
        return new ResponseEntity<>(listadoTotalUsuarios.stream().
                map(userFactoryImp::crearUserDTO).collect(Collectors.toList()), HttpStatus.OK);
    }

    /**
     * Endpoint que devuelve usuario determinado por id
     * @param id -
     * @return ResponseEntity<List<UserDTO>>
     */
    @Operation(summary = "Mostrar usuarios según id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuario encontrado",content = {@Content(mediaType="application/json",schema=@Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Usuario no encontrado", content = @Content)
    })

    @GetMapping("/usuarios/{id}")
    public ResponseEntity<UserDTO> buscarUsuarioId(@Validated @PathVariable int id){
        Optional<User> userOpt = userServiceImp.findUserById(id);

        return new ResponseEntity<>(userOpt.map(userFactoryImp::crearUserDTO).
                orElseThrow(()->new UserNotFoundException("No se ha encontrado usuario con el siguiente id: " + id)),
                HttpStatus.OK);
    }

    /**
     * Endpoint encargado de la creación y guardado de un usuario
     * @param userDTO -
     * @return ResponseEntity<User>
     */
    @Operation(summary = "Crear y guardar usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuario creado",content = {@Content(mediaType="application/json",schema=@Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "500", description = "Usuario creado con campo id", content = @Content)
    })

    @PostMapping("/usuario")
    public ResponseEntity<User> crearUsuario(@Validated @RequestBody UserDTO userDTO){
        User usuario = dtoConverter.convertDTOToEntity(userDTO, User.class);

        if(usuario.getIdUsuario()!=0){
            throw new UserGlobalException("El campo del id debe estar vacío");
        }
        return ResponseEntity.ok(userServiceImp.saveUser(usuario));

    }

    /**
     * Endpoint encargado de actualizar y guardar usuario ya existente
     * @param userDTO -
     * @return ResponseEntity<UserDTO>
     */

    @Operation(summary = "Actualizar y guardar usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuario actualizado y, si no existe, usuario nuevo creado",content = {@Content(mediaType="application/json",schema=@Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "400", description = "No se ha podido llevar a cabo la actualización", content = @Content)
    })

    @PutMapping("/usuario")
    public ResponseEntity<UserDTO> actualizarUser(@RequestBody UserDTO userDTO){
        if(userDTO.getIdUsuario()<=0){
            throw new UserBadRequestException("No se ha podido atender la petición de actualización");
        }
        User usuario=dtoConverter.convertDTOToEntity(userDTO, User.class);
        userServiceImp.saveUser(usuario);
        return ResponseEntity.ok(dtoConverter.convertEntityToDTO(usuario, UserDTO.class));
    }

    /**
     * Endpoint encargado de borrar todos los usuarios
     * @return ResponseEntity<HttpStatus>
     */

    @Operation(summary = "Eliminar listado de usuarios")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Listado borrado correctamente",content = @Content),
            @ApiResponse(responseCode = "500", description = "No se ha podido borrar el listado de usuarios", content = @Content)
    })

    @DeleteMapping("/usuarios")
    public ResponseEntity<HttpStatus> borrarTodosUsuarios(){
        boolean result = userServiceImp.deleteAllUsers();
        if(result){
            throw new UserNoContentException("La lista se ha borrado correctamente");
        }
        throw new UserGlobalException("Error al borrar todos los usuarios");
    }

    /**
     * Endpoint encargado de borrar usuario determinado según id
     * @param id
     * @return ResponseEntity<HttpStatus>
     */

    @Operation(summary = "Eliminar usuario por id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Usuario borrado correctamente",content = @Content),
            @ApiResponse(responseCode = "404", description = "No se ha encontrado usuario con el id solicitado y, por lo tanto, no puede borrarse", content = @Content)
    })

    @DeleteMapping("/usuarios/{id}")
    public ResponseEntity<HttpStatus> borrarUsuarioPorId(@Validated @PathVariable int id){
        boolean result=userServiceImp.deleteUserById(id);
        if(result){
            throw new UserNoContentException("Se ha borrado correctamente el usuario con el id: " + id);
        }
        throw new UserNotFoundException("No se ha encontrado el usuario con el id: " + id);
    }

}
