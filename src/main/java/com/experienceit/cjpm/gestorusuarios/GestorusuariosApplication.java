package com.experienceit.cjpm.gestorusuarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestorusuariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestorusuariosApplication.class, args);
	}

}
