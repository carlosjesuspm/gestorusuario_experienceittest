package com.experienceit.cjpm.gestorusuarios.exception;

import java.io.Serial;

public class UserBadRequestException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 1L;

    public UserBadRequestException(String msg){
        super(msg);
    }
}
