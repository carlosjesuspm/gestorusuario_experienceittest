package com.experienceit.cjpm.gestorusuarios.exception;

import java.io.Serial;

public class UserGlobalException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 1L;

    public UserGlobalException(String msg){
        super(msg);
    }
}
