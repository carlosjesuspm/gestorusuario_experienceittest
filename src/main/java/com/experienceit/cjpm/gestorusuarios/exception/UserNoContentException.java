package com.experienceit.cjpm.gestorusuarios.exception;

import java.io.Serial;

public class UserNoContentException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public UserNoContentException(String msg){
        super(msg);
    }
}
