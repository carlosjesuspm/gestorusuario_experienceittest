package com.experienceit.cjpm.gestorusuarios.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class ControllerExceptionHandler {

    /**
     * Método encargado de capturar error NotFound
     * @param ex -
     * @param request -
     * @return ResponseEntity<MensajeErrorUsuario>
     */
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<MensajeErrorUser> usuarioNotFoundException(UserNotFoundException ex, WebRequest request){
        MensajeErrorUser mensajeErrorPersonalizado = new MensajeErrorUser(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<MensajeErrorUser>(mensajeErrorPersonalizado, HttpStatus.NOT_FOUND);
    }

    /**
     * Método encargado de capturar error BadRequest
     * @param ex -
     * @param request -
     * @return ResponseEntity<MensajeErrorUser>
     */
    @ExceptionHandler(UserBadRequestException.class)
    public ResponseEntity<MensajeErrorUser> userBadRequestException(UserBadRequestException ex, WebRequest request){
        MensajeErrorUser mensajeErrorPersonalizado = new MensajeErrorUser(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<MensajeErrorUser>(mensajeErrorPersonalizado, HttpStatus.BAD_REQUEST);
    }

    /**
     * Método encargado de capturar error GlobalExceptionHandler
     * @param ex -
     * @param request -
     * @return ResponseEntity<MensajeErrorUser>
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<MensajeErrorUser> globalExceptionHandler(Exception ex, WebRequest request){
        MensajeErrorUser mensajeErrorPersonalizado = new MensajeErrorUser(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));
        return new ResponseEntity<MensajeErrorUser>(mensajeErrorPersonalizado, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Método encargado de capturar error NoContent
     * @param ex -
     * @param request -
     * @return ResponseEntity<MensajeErrorUser>
     */
    @ExceptionHandler(UserNoContentException.class)
    public ResponseEntity<MensajeErrorUser> userNoContentException(UserNoContentException ex, WebRequest request){
        MensajeErrorUser mensajeErrorPersonalizado = new MensajeErrorUser(
                HttpStatus.NO_CONTENT.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<MensajeErrorUser>(mensajeErrorPersonalizado, HttpStatus.NO_CONTENT);
    }
}
