package com.experienceit.cjpm.gestorusuarios.repository;

import com.experienceit.cjpm.gestorusuarios.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repositorio para usuario
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
